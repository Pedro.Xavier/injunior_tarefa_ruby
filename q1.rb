require "json"

file = File.read('./pokemons.json')
dados = JSON.parse(file)

pokemons = dados["results"]

q1 = []

for i in pokemons
    x = 0
    c = 0
    while c <= i["name"].length
        if (i["name"][c] == '-')
            x = c
            break
        end
        c += 1
    end
    if(i["name"].include? '-')
        nome = i["name"].slice(0, x)
    else
        nome = i["name"]
    end

    if(!q1.include? nome)
        q1.append(nome)
    end

end

puts q1
puts "\nExistem #{q1.length} pokemons distintos."

