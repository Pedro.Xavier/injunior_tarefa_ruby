require "json"

file = File.read('./pokemons.json')
dados = JSON.parse(file)

pokemons = dados["results"]

repetidos = []
aux = []

for i in pokemons
    x = 0
    c = 0
    while c <= i["name"].length
        if (i["name"][c] == '-')
            x = c
            break
        end
        c += 1
    end
    if(i["name"].include? '-')
        nome = i["name"].slice(0, x)
    else
        nome = i["name"]
    end

    aux.append(nome)

end

for i in aux
    if (aux.count(i) > 1)
        repetidos.append(i)
    end
end

repetidos = repetidos.inject(Hash.new(0)) {|memo,value| memo[value] += 1; memo}.sort_by{|k, v| v}.reverse

puts "Top 10 pokemons que mais se repetem: "
for i in 0..10
    puts "#{repetidos[i][0]}: #{repetidos[i][1]} vezes"
end



